# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).


## 0.5.0 - 2018.09.15

### Changed
 - skip greenindex bd-test to wait for new google map api key
   [BD-2161](https://opensource.ncsa.illinois.edu/jira/browse/BD-2161)
   [BD-2151](https://opensource.ncsa.illinois.edu/jira/browse/BD-2151)

### Fixed
 - fix timeout exception
   [BD-2150](https://opensource.ncsa.illinois.edu/jira/browse/BD-2150)
   [BD-2169](https://opensource.ncsa.illinois.edu/jira/browse/BD-2169)

## 0.4.1 - 2018.06.15

### Changed
 - skip humanpref bd-test to wait for deploy
   [BD-2153](https://opensource.ncsa.illinois.edu/jira/browse/BD-2153)

## 0.4.0 - 2018.05.15

### Changed
 - update bd-test case of greenindex extractor on dev. 
   [BD-2118](https://opensource.ncsa.illinois.edu/jira/browse/BD-2118)

### Added
 - Add testcase extractor humanpref.
   [BD-1948](https://opensource.ncsa.illinois.edu/jira/browse/BD-1948)


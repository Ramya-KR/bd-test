import json
import pytest
from util import *


# @pytest.mark.skip(reason="testing conversions")
def test_get_extract(host, api_token, request_timeout, processing_timeout, extraction_data):
    # should this test be skipped
    if 'skip' in extraction_data:
        pytest.skip(extraction_data['skip'])

    if 'file_url' in extraction_data:
        extract(host, api_token, request_timeout, processing_timeout, extraction_data, 'file_url')
    if 'download' in extraction_data:
        extraction_data['file_path'] = download_file_web(extraction_data['file_url'])
        extract(host, api_token, request_timeout, processing_timeout, extraction_data, 'file_path')


def extract(host, api_token, request_timeout, processing_timeout, extraction_data, file_field):
    print "Description     :", extraction_data['description']
    print "Extractor       :", extraction_data.get('extractor', 'all')
    print "Extracting from :", extraction_data[file_field]
    print "Expecting       :", extraction_data['output']

    input_url = extraction_data[file_field]
    output = extraction_data['output']
    try:
        metadata = extract_func(host, api_token, input_url, extraction_data.get('extractor', 'all'),
                                request_timeout, processing_timeout)
    except requests.HTTPError as e:
        print("Exception in extracting metadata code=%d msg='%s'" % (e.response.status_code, e.response.text))
        raise e
    finally:
        # remove downloaded temporary file
        filename = extraction_data[file_field]
        if os.path.isfile(filename):
            os.remove(filename)

    print("Extraction output " + metadata)
    if output.startswith("http://") or output.startswith("https://"):
        output = urllib2.urlopen(output).read().strip()
    assert metadata.find(output) != -1, "Could not find expected text"


def extract_func(endpoint, api_token, input_url, extractor, request_timeout, processing_timeout):
    metadata = []
    stoptime = time.time() + processing_timeout
    headers_json = {'Authorization': api_token, 'Content-Type': 'application/json'}
    headers_del = {'Authorization': api_token}

    try:
        # upload the file
        if os.path.exists(input_url):
            api_call_url = endpoint + '/extractions/file'
            if extractor != 'all':
                api_call_url += '?extract=0'
            print "API Call        :", api_call_url
            boundary = 'browndog-fence-header'
            files = [('File', (input_url, mimetypes.guess_type(input_url)[0] or 'application/octet-stream'))]
            r = requests.post(api_call_url,
                              headers={'Accept': 'application/json', 'Authorization': api_token,
                                       'Content-Type': 'multipart/form-data; boundary=' + boundary},
                              timeout=request_timeout,
                              data=multipart([], files, boundary, 5 * 1024 * 1024))
        else:
            api_call_file = endpoint + '/extractions/url'
            if extractor != 'all':
                api_call_file += '?extract=0'
            print "API Call        :", api_call_file
            r = requests.post(api_call_file, headers=headers_json, timeout=request_timeout,
                              data=json.dumps({"fileurl": input_url}))
        r.raise_for_status()
        file_id = r.json()['id']
        print("File id " + file_id)

        # trigger extractor, and check specific output
        if extractor != 'all':
            # Wait for the file upload to complete before issuing extraction request
            file_uploaded = False
            while not file_uploaded and stoptime > time.time():
                r = requests.get("%s/extractions/files/%s" % (endpoint, file_id), headers=headers_json,
                                 timeout=request_timeout)
                r.raise_for_status()
                if r.json()['status'] == "PROCESSED":
                    file_uploaded = True
                time.sleep(1)

            if stoptime <= time.time():
                raise requests.Timeout("process timeout on waiting for file upload")

            if file_uploaded:
                # Submit file for extraction
                r = requests.post("%s/extractions/files/%s" % (endpoint, file_id), headers=headers_json,
                                  timeout=request_timeout, data=json.dumps({"extractor": extractor}))
                r.raise_for_status()

                # Wait for the right metadata to be ready
                while stoptime > time.time():
                    r = requests.get('%s/extractions/files/%s/metadata.jsonld?extractor=%s' %
                                     (endpoint, file_id, extractor), headers=headers_json, timeout=request_timeout)
                    r.raise_for_status()
                    if r.text != '[]':
                        metadata = r.json()
                        break
                    time.sleep(1)

                if stoptime <= time.time():
                    raise requests.Timeout("process timeout on waiting metadata to be ready")
        else:
            # Poll until output is ready (optional)
            while stoptime > time.time():
                r = requests.get(endpoint + '/extractions/' + file_id + '/status', headers=headers_json,
                                 timeout=request_timeout)
                r.raise_for_status()
                status = r.json()
                if status['Status'] == 'Done':
                    print("Status: Done")
                    break
                time.sleep(1)

            if stoptime <= time.time():
                raise requests.Timeout("process timeout on polling extractions status")

            # Display extracted content
            r = requests.get(endpoint + '/extractions/files/' + file_id + '/metadata.jsonld', headers=headers_json,
                             timeout=request_timeout)
            r.raise_for_status()
            metadata = r.json()

        # Delete test files
        r = requests.delete(endpoint + '/extractions/files/' + file_id, data={}, headers=headers_del,
                            timeout=request_timeout)
        r.raise_for_status()

    except Exception as e:
        raise e
    finally:
        try:
            requests.delete(endpoint + '/extractions/files/' + file_id, data={}, headers=headers_del,
                            timeout=request_timeout)
        except Exception as e:  # noinspection PyBroadException
            pass
    return json.dumps(metadata)
